﻿using IgniaTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IgniaTest.Interfaces
{
    public interface IWeatherClient
    {
        Task<List<CityModel>> SearchByCountryAsync(string countryId, string cityId);       
        Task<List<WeatherModel>> GetCurrentWeatherByLocationKey(string locationKey);
        List<Tuple<string, string>> GetCountries();
        //Task<List<LocationModel>> GetAllLocationsAsync();
        //Task<List<LocationModel>> GetAllCountriesAsync();


    }
}
