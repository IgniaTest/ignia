﻿using IgniaTest.Interfaces;
using IgniaTest.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System;
using System.Globalization;

namespace IgniaTest.Clients
{
    public class AccuWeatherClient : IWeatherClient
    {
        //these are included in the data that comes from Accuweather but the RegionInfo does not accept
        //the codes as a constructor parameter. This is a workaround
        private readonly Dictionary<string, string> _unknownCountryIds = new Dictionary<string, string>
        {
            {"AQ", "Antarctica" },
            {"BV", " Bouvet Island" },
            {"EH", "Western Sahara" },
            {"GS", "South Georgia and the South Sandwich Islands" },
            {"HM", "Heard Island and McDonald Islands" },
            {"TF", "French Southern Territories" }
        };

        private readonly string _apiKey = "apikey=fzkjzKN20HU7vLQpQYXPd0voU4zaFuc5"; //"apikey=mHiKGUruO0b9dDAVC3v2VFUVbUF2aqyy"
        private readonly HttpClient _httpClient = new HttpClient();

        public async Task<List<CityModel>> SearchByCountryAsync(string countryId, string cityName)
        {
            var response = await _httpClient.GetAsync($"http://dataservice.accuweather.com/locations/v1/{countryId}/search?{_apiKey}&q={cityName}");           
            return JsonConvert.DeserializeObject<List<CityModel>>(await response.Content.ReadAsStringAsync());
        }          

    
        //The free API only allows 50 calls per day I was retreiving the data from the server
        //but I hit 50 calls very quickly doing that. So build it manually.
        public List<Tuple<string, string>> GetCountries()
        {
            List<Tuple<string, string>> countries = new List<Tuple<string, string>>();
            foreach(var name in Enum.GetNames(typeof(CountryEnum)))
            {
                if (_unknownCountryIds.ContainsKey(name))
                {
                    countries.Add(new Tuple<string, string>(name, _unknownCountryIds[name]));
                }
                else
                {
                    RegionInfo info = new RegionInfo(name);
                    countries.Add(new Tuple<string, string>(name, info.EnglishName));
                }
            }

            return countries.OrderBy(x => x.Item2).ToList();
        }

        public async Task<List<WeatherModel>> GetCurrentWeatherByLocationKey(string locationKey)
        {
            var response = await _httpClient.GetAsync($"http://dataservice.accuweather.com/currentconditions/v1/{locationKey}?{_apiKey}");
            return JsonConvert.DeserializeObject<List<WeatherModel>>(await response.Content.ReadAsStringAsync());
        }

        /* 
             public async Task<List<LocationModel>> GetAllLocationsAsync()
         {
             var response = await _httpClient.GetAsync($"http://dataservice.accuweather.com/locations/v1/regions?{_apiKey}");
             return JsonConvert.DeserializeObject<List<LocationModel>>(await response.Content.ReadAsStringAsync());
         }

          public async Task<List<LocationModel>> GetAllCountriesAsync()
         {
             List<LocationModel> locations = await GetAllLocationsAsync();
             List<string> urls = CreateUrls(locations, "countries");
             List<Task<List<LocationModel>>> tasks = (from url in urls
                                                      select GetCountriesByRegion(url)).ToList();

             List<LocationModel> countries = await ExecuteTasks(tasks);
             return countries.OrderBy(x => x.EnglishName).ToList();
         }   

         private async Task<List<LocationModel>> ExecuteTasks(List<Task<List<LocationModel>>> tasks)
         {
             List<LocationModel> list = new List<LocationModel>();
             while (tasks.Any())
             {
                 Task<List<LocationModel>> completedTask = await Task.WhenAny(tasks);
                 tasks.Remove(completedTask);
                 list.AddRange(await completedTask);
             }

             return list;
         }

         private async Task<List<LocationModel>> GetCountriesByRegion(string url)
         {
             var response = await _httpClient.GetAsync(url);
             return JsonConvert.DeserializeObject<List<LocationModel>>(await response.Content.ReadAsStringAsync());
         }

         private List<string> CreateUrls(List<LocationModel> locations, string api)
         {
             List<string> urls = new List<string>();
             foreach (var location in locations.Select(x => x.ID))
             {
                 urls.Add($"http://dataservice.accuweather.com/locations/v1/{api}/{location}?{_apiKey}");
             }
             return urls;
         }*/
    }
}