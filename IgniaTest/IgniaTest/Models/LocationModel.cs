﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgniaTest.Models
{
    public class LocationModel
    {
        public string ID { get; set; }
        public string LocalizedName { get; set; }
        public string EnglishName { get; set; }
    }
}