﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgniaTest.Models
{
    public class TempBase
    {
        public double Value { get; set; }
        public string Unit { get; set; }
        public int UnitType { get; set; }
    }

    public class Metric :TempBase
    {
        public string GetTemp()
        {
            return $"{Value.ToString()} {Unit}"; 
        }
    }
    public class Imperial : TempBase
    {

       
    }

    public class TemperatureModel
    {
        public Metric Metric { get; set; }
        public Imperial Imperial { get; set; }
    }

    public class WeatherModel
    {
        public string LocalObservationDateTime { get; set; }
        public long EpochTime { get; set; }
        public string WeatherText { get; set; }
        public string WeatherIcon { get; set; }
        public bool  HasPrecipitation { get; set; }
        public string PrecipitationType { get; set; }
        public bool IsDayTime { get; set; }
        public TemperatureModel Temperature { get; set; }
        public string MobileLink { get; set; }
        public string Link { get; set; }
    }

   
    /*[
  {
    "LocalObservationDateTime": "2019-01-17T14:22:00+08:00",
    "EpochTime": 1547706120,
    "WeatherText": "Sunny",
    "": 1,
    "HasPrecipitation": false,
    "PrecipitationType": null,
    "IsDayTime": true,
    "Temperature": {
      "Metric": {
        "Value": 25,
        "Unit": "C",
        "UnitType": 17
      },
      "Imperial": {
        "Value": 77,
        "Unit": "F",
        "UnitType": 18
      }
    },
    "MobileLink": "http://m.accuweather.com/en/au/perth/26797/current-weather/26797?lang=en-us",
    "Link": "http://www.accuweather.com/en/au/perth/26797/current-weather/26797?lang=en-us"
  }
]*/
}