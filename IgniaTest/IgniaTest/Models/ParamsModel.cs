﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgniaTest.Models
{
    public class ParamsModel
    {
        public string CountryId { get; set; }
        public string CityName { get; set; }
        public string AdminCode { get; set; }
        public string Key { get; set; }
    }
}