﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IgniaTest.Models
{
    public class CityModel
    {
        public string Key { get; set; }
        public string LocalizedName { get; set; }
        public string EnglishName { get; set; }

        public AdministrativeArea AdministrativeArea {get;set;}
        public bool NotFound { get; set; }
    }

    public class AdministrativeArea
    {
        public string ID { get; set; }
        public string LocalizedName { get; set; }
        public string EnglishName { get; set; }
    }
        /*[
      {
        "Version": 1,
        "Key": "26797",
        "Type": "City",
        "Rank": 21,
        "LocalizedName": "Perth",
        "EnglishName": "Perth",
        "PrimaryPostalCode": "",
        "Region": {
          "ID": "OCN",
          "LocalizedName": "Oceania",
          "EnglishName": "Oceania"
        },
        "Country": {
          "ID": "AU",
          "LocalizedName": "Australia",
          "EnglishName": "Australia"
        },
        "AdministrativeArea": {
          "ID": "WA",
          "LocalizedName": "Western Australia",
          "EnglishName": "Western Australia",
          "Level": 1,
          "LocalizedType": "State",
          "EnglishType": "State",
          "CountryID": "AU"
        },
        "TimeZone": {
          "Code": "AWST",
          "Name": "Australia/Perth",
          "GmtOffset": 8,
          "IsDaylightSaving": false,
          "NextOffsetChange": null
        },
        "GeoPosition": {
          "Latitude": -31.953,
          "Longitude": 115.861,
          "Elevation": {
            "Metric": {
              "Value": 46,
              "Unit": "m",
              "UnitType": 5
            },
            "Imperial": {
              "Value": 150,
              "Unit": "ft",
              "UnitType": 0
            }
          }
        },
        "IsAlias": false,
        "SupplementalAdminAreas": [],
        "DataSets": []
      },
      {
        "Version": 1,
        "Key": "25571",
        "Type": "City",
        "Rank": 85,
        "LocalizedName": "Perth",
        "EnglishName": "Perth",
        "PrimaryPostalCode": "",
        "Region": {
          "ID": "OCN",
          "LocalizedName": "Oceania",
          "EnglishName": "Oceania"
        },
        "Country": {
          "ID": "AU",
          "LocalizedName": "Australia",
          "EnglishName": "Australia"
        },
        "AdministrativeArea": {
          "ID": "TAS",
          "LocalizedName": "Tasmania",
          "EnglishName": "Tasmania",
          "Level": 1,
          "LocalizedType": "State",
          "EnglishType": "State",
          "CountryID": "AU"
        },
        "TimeZone": {
          "Code": "AEDT",
          "Name": "Australia/Hobart",
          "GmtOffset": 11,
          "IsDaylightSaving": true,
          "NextOffsetChange": "2019-04-06T16:00:00Z"
        },
        "GeoPosition": {
          "Latitude": -41.577,
          "Longitude": 147.159,
          "Elevation": {
            "Metric": {
              "Value": 87,
              "Unit": "m",
              "UnitType": 5
            },
            "Imperial": {
              "Value": 285,
              "Unit": "ft",
              "UnitType": 0
            }
          }
        },
        "IsAlias": false,
        "SupplementalAdminAreas": [
          {
            "Level": 2,
            "LocalizedName": "Northern Midlands",
            "EnglishName": "Northern Midlands"
          }
        ],
        "DataSets": []
      }
    ]*/
    }