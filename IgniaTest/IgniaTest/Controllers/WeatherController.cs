﻿using IgniaTest.Clients;
using IgniaTest.Interfaces;
using IgniaTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IgniaTest.Controllers
{
   
    public class WeatherController : Controller
    {
        private readonly IWeatherClient _weatherClient = null;

        public WeatherController(IWeatherClient weatherClient)
        {
            _weatherClient = weatherClient;
        }

        public WeatherController()
        {
            _weatherClient = new AccuWeatherClient();
        }

        // GET: Weather
        public ActionResult Index()
        {
            var countries = _weatherClient.GetCountries();
            List<SelectListItem> items = new List<SelectListItem>(countries.Select(x => new SelectListItem { Value = x.Item1, Text = x.Item2 }));

            return View(items);
        }

        public async Task<ActionResult> GetCurrentWeather(ParamsModel model)
        {
            var searchResult = await _weatherClient.SearchByCountryAsync(model.CountryId, model.CityName);

            if (!searchResult.Any())
            {
                return PartialView("_ErrorMessage", $"Could not find the city {model.CityName}. Have you selected the correct country?");
            }
            else if(searchResult.Count > 1)
            {
                //send back dialog
                return CreateDialogPartial(searchResult);
            }

            var weatherResult = await _weatherClient.GetCurrentWeatherByLocationKey(searchResult.First().Key);
            return PartialView("_DisplayWeather",  weatherResult.First());
        }
        
        public async Task<ActionResult> GetCurrentWeatherDialog(ParamsModel model)
        {
            var weatherResult = await _weatherClient.GetCurrentWeatherByLocationKey(model.Key);
            return PartialView("_DisplayWeather", weatherResult.First());
        }

        private ActionResult CreateDialogPartial(List<CityModel> models)
        {
            List<SelectListItem> items = new List<SelectListItem>(models.Select(x => new SelectListItem { Value = x.Key, Text = $"{x.EnglishName}, {x.AdministrativeArea.EnglishName}" }));
            return PartialView("_Dialog", items);
        }
    }
}