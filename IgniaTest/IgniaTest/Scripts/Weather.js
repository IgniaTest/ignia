﻿$(function () {
    $("#getWeather").on("click", function () {
        getCurrentWeather();
    });      

    var defaultLocation = localStorage.getItem("defaultLocation");
    if (defaultLocation != undefined) {
        var location = JSON.parse(defaultLocation);
        $("#countries").val(location.countryId);
        $("#citySearch").val(location.cityName);
    }
});

var getCurrentWeather = function () {

    var country = $("#countries").val();
    var city = $("#citySearch").val();

    var searchData = { countryId: country, cityName: city };
    $.ajax({
        method: "POST",
        url: "/Weather/GetCurrentWeather",
        data: searchData
    }).done(function (data) {
        data = $.trim(data);
        if (data.indexOf("dialogDiv") > -1) {
            createDialog(data);
        }
        else if (data.indexOf("errorMessage") > -1) {
            displayErrorMessage(data);
        }
        else {
            $("#partialDiv").html(data);
            localStorage.setItem("defaultLocation", JSON.stringify(searchData));
        }
    });
}

var createDialog = function (html) {
    var searchButton = {
        text: "Search",
        click: searchDialog
    }

    var cancelButton = {
        text: "Cancel",
        click: closeDialog
    }

    $("#dialog-modal").html(html)
    $("#dialog-modal").dialog(
        {
            buttons: [searchButton, cancelButton],
            height: 250,
            width: 250,
            modal: true,
            title: "Choose City"
        });
}

var searchDialog = function () {
    var key = $("#cities").val();
    var city = $("#cities option:selected").text();
    $.ajax({
        method: "POST",
        url: "/Weather/GetCurrentWeatherDialog",
        data: { Key: key }
    })
        .done(function (data) {
            data = $.trim(data);
            $("#partialDiv").html(data);
            localStorage.setItem("defaultLocation",
                JSON.stringify({
                    countryId: $("#countries").val(),
                    cityName: getCityName(city)
                }));
        });
    closeDialog();
}

var getCityName = function (cityName) {
    return cityName.split(",")[0];
}

var closeDialog = function () {
    $("#dialog-modal").dialog("close");
}

var displayErrorMessage = function (message) {
    alert($(message).text());
}